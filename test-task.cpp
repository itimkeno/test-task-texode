﻿
#include "tariffs.h"
#include "plan.h"
#include "loader_tariffs.h"
#include <iostream>


#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string_view>
#include <vector>

bool check_minutes_tariff(const std::vector<tariff> &tariffs,
                          const plan &task) {
  double optimal_use_minute = 0;
  size_t min_use = 0;
  while (optimal_use_minute < tariffs[1].cost_tariff) {
    optimal_use_minute = tariffs[0].cost_time_use * min_use;
    ++min_use;
  }
  std::cout << std::endl << min_use << std::endl;
  if (task.time_use_sum > min_use)
    std::cout << tariffs[0].name << " is not optimal for use" << std::endl;

  double optimal_standby_minute = 0;
  size_t min_stand = 0;
  while (optimal_standby_minute < tariffs[1].cost_tariff) {
    optimal_standby_minute = tariffs[0].cost_time_out * min_stand;
    ++min_stand;
  }
  std::cout << std::endl << min_stand << std::endl;

  if (task.time_out_sum > min_stand)
    std::cout << tariffs[0].name << " is not optimal for standby" << std::endl;

  return false;
}

std::string find_optimal(const std::vector<tariff> &tariffs, const plan &task) {

  std::string str;
  std::string is_optimal;
  double best_cost = 1000; // wtf

  for (size_t k = 0; k < task.tasks.size(); ++k) // first
  {

    for (tariff tarif : tariffs) // chek cost
    {
      double cost_tmp = 0;
      cost_tmp += task.tasks[k].time_use * tarif.cost_time_use +
                  task.tasks[k].time_out * tarif.cost_time_out;

      if ((task.tasks[k].time_out + task.tasks[k].time_use) <=
          tarif.included_time) {
        cost_tmp += tarif.cost_tariff;

        if (task.tasks[k].distance > tarif.distance) {
          cost_tmp += (task.tasks[k].distance - tarif.distance) *
                      tarif.cost_over_trafic;
        }
      }

      //std::cout << tarif.name << " cost:  " << cost_tmp << std::endl;

      if (cost_tmp != 0.0 && cost_tmp <= best_cost) {
        best_cost = cost_tmp;
        is_optimal = tarif.name;
      }
    }

   // std::cout << " price " << best_cost << std::endl;
    str +=
        "best price: " + std::to_string(best_cost) + " by " + is_optimal + "\n";
    std::cout << std::endl;
  }
  return str;
}

int main() {
  std::vector<tariff> tariffs;
  std::stringstream file_tariffs = read_tariffs_file("data/tariffs.txt");

  std::copy_n(std::istream_iterator<tariff>(file_tariffs), 6,
              std::back_inserter(tariffs));

  plan pln;
  pln.read_plan_file("data/plan.txt");

  std::cout << std::endl;

  std::ofstream answ;
  answ.open("data/result.txt", std::ofstream::out);
  if (!answ.is_open()) {
    std::cerr << "cant open file";
  } else {
    answ << find_optimal(tariffs, pln);
  }
  answ.close();

  return 0;
}

