#pragma once
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string_view>
#include <string>
#include <vector>

struct plan {
  struct one_task {
    one_task() {
      distance = 0;
      time_use = 0;
      time_out = 0;
    }
    double distance;
    double time_use;
    double time_out;
  };

  double distance_sum = 0;
  double time_use_sum = 0;
  double time_out_sum = 0; // standby
  double time_all = 0;
  std::vector<one_task> tasks;

  void read_plan_file(std::string_view path);
};
