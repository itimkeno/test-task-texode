#include "plan.h"

void plan::read_plan_file(std::string_view path) {
  std::ifstream file_plan(path.data(), std::ios_base::binary);

  if (!file_plan) {
    std::cerr << (std::string("can't load ") + path.data()) << std::endl;
  }

  std::string line;
  std::string drive = "drive";     // mark word
  std::string standby = "standby"; // mark word

  while (std::getline(file_plan, line)) {
    one_task task;
    size_t mark_time_minutes = line.find(" minutes");
    size_t mark_time_minute = line.find(" minute.");
    size_t mark_time_hours = line.find(" hours");
    size_t mark_time_hour = line.find(" hour.");
    size_t mark_dist = line.find(" km");
    size_t shift = 0; // shift (error) while substring have numbers

    if (line.find(drive) != std::string::npos) {
      if (mark_dist != std::string::npos) {
        if (isdigit(line[mark_dist - 1])) {
          for (size_t i = mark_dist - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.distance = atof(&line[shift]);
          distance_sum += atof(&line[shift]);
        }
      }

      if (mark_time_minutes != std::string::npos) {
        if (isdigit(line[mark_time_minutes - 1])) {
          for (size_t i = mark_time_minutes - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_use = atof(&line[shift]);
          time_use_sum += atof(&line[shift]);
        }
      }
      if (mark_time_minute != std::string::npos) {
        if (isdigit(line[mark_time_minute - 1])) {
          for (size_t i = mark_time_minute - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_use = atof(&line[shift]);
          time_use_sum += atof(&line[shift]);
        }
      }

      if (mark_time_hours != std::string::npos) {
        if (isdigit(line[mark_time_hours - 1])) {
          for (size_t i = mark_time_hours - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_use = 60 * atof(&line[shift]);
          time_use_sum += 60 * atof(&line[shift]);
        }
      }
      if (mark_time_hour != std::string::npos) {
        if (isdigit(line[mark_time_hour - 1])) {
          for (size_t i = mark_time_hour - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_use = 60 * atof(&line[shift]);
          time_use_sum += 60 * atof(&line[shift]);
        }
      }
    }

    if (line.find(standby) != std::string::npos) {
      if (mark_time_minutes != std::string::npos) {
        if (isdigit(line[mark_time_minutes - 1])) {
          for (size_t i = mark_time_minutes - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_out = atof(&line[shift]);
          time_out_sum += atof(&line[shift]);
        }
      }
      if (mark_time_minute != std::string::npos) {
        if (isdigit(line[mark_time_minute - 1])) {
          for (size_t i = mark_time_minute - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_out = atof(&line[shift]);
          time_out_sum += atof(&line[shift]);
        }
      }

      if (mark_time_hours != std::string::npos) {
        if (isdigit(line[mark_time_hours - 1])) {
          for (size_t i = mark_time_hours - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_out = 60 * atof(&line[shift]);
          time_out_sum += 60 * atof(&line[shift]);
        }
      }
      if (mark_time_hour != std::string::npos) {
        if (isdigit(line[mark_time_hour - 1])) {
          for (size_t i = mark_time_hour - 1; isdigit(line[i]); i--) {
            shift = i;
          }
          task.time_out = 60 * atof(&line[shift]);
          time_out_sum += 60 * atof(&line[shift]);
        }
      }
    }
    tasks.push_back(task);
  }

  time_all = time_use_sum + time_out_sum;

  file_plan.close();
}