#include "tariffs.h"

std::istream &operator>>(std::istream &stream, tariff &obj) {
  // std::getline(stream, obj.name, '|');
  stream >> obj.name;
  stream >> obj.type;
  stream >> obj.cost_time_use;
  stream >> obj.cost_time_out;

  double hours = 0;
  stream >> hours;
  obj.included_time = hours * 60;

  stream >> obj.cost_tariff;
  stream >> obj.distance;
  stream >> obj.cost_over_trafic;

  return stream;
}

std::istream &operator>>(std::istream &stream, tariff_type &type) {
  static const std::map<std::string, tariff_type> types = {
      {"minute", tariff_type::minute},
      {"three_hours", tariff_type::three_hours},
      {"six_hours", tariff_type::six_hours},
      {"nine_hours", tariff_type::nine_hours},
      {"twelve_hours", tariff_type::twelve_hours},
      {"one_day", tariff_type::one_day},
  };

  std::string type_name;
  stream >> type_name;

  auto it = types.find(type_name);

  if (it != end(types)) {
    type = it->second;
  } else {
    std::stringstream ss;
    ss << "expected one of: ";
    std::for_each(begin(types), end(types),
                  [&ss](const auto &kv) { ss << kv.first << ", "; });
    ss << " but got: " << type_name;
    std::cerr << (ss.str());
  }

  return stream;
}