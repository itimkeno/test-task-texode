#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>

                std::stringstream
                read_tariffs_file(std::string_view file) {
  std::stringstream out;
  std::string line;
  std::ifstream in(file.data(), std::ios_base::binary);

  if (!in) {
    std::cerr << (std::string(" can't open file: ") + file.data()) << std::endl;
    // throw std::runtime_error(std::string("can't open file: ") +
    // file.data());
  }

  while (std::getline(in, line)) {
    size_t comment_pos = line.find("//");
    if (comment_pos != std::string::npos) {
      line = line.substr(0, comment_pos);
    }
    if (!line.empty()) {
      out << line << '\n';
    }
  }
  in.close();

  return out;
}