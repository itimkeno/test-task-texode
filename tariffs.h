#pragma once
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

enum class tariff_type {
  minute,
  three_hours,
  six_hours,
  nine_hours,
  twelve_hours,
  one_day
};

struct tariff {
  std::string name;
  enum tariff_type type;

  double cost_time_use;    // BYN / min
  double cost_time_out;    // BYN / min // standby
  double included_time;    // minutes
  double cost_tariff;      // BYN
  double distance;         // km
  double cost_over_trafic; // BYN / min

  // double total_cost; // BYN
  // double total_time;

  double optimal_use_minutes();
  double optimal_standby_minutes();
};

extern std::istream &operator>>(std::istream &stream, tariff_type &type);

extern std::istream &operator>>(std::istream &stream, tariff &obj);